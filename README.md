# Artefacto informatico KBS

El artefacto informático que contendrá el conocimiento del dominio, será una aplicación web. La
aplicación web contendrá un repositorio de información, donde los usuarios podrán encontrar
respuesta a las preguntas que se han propuesto a resolver con base en la ontología desarrollada.
El conocimiento alojado en este artefacto, es generado gracias a la experiencia de empleados del
área CPGs y Growth de Rappi, los cuales tienen experiencia en la toma de decisiones con base a
métricas cómo: GMV (Gross Merchandise Value) generado por tienda, número de ordenes diarias
por tienda, AOV (Average Order Value), CAC (Customer Acquisition Cost), entre otras, las cuales
son obtenidas, luego de que los datos crudos pasan por un proceso ETL y se convierten explícitos
en un dashboard de control en la herramienta PowerBI.

Se decidió escoger una aplicación web por las siguientes razones:

1. Nuevas tecnologías permiten un rápido desarrollo del artefacto.
2. Multi-plataforma al correr en un navegador.
3. Fácil acceso al necesitar únicamente la URL de la página web.
4. No hay necesidad de configuración alguna.

## Tecnologías a utilizar:

Para programar el front-end de la aplicación, se escogieron las siguientes tecnologías:

1. Javascript. Lenguaje para programar interacción del usuario con la herramienta
2. HTML
3. React como framework front-end, el cual ofrece reutilización de componentes visuales para
   un rápido desarrollo.

Para programar el back-end de la aplicación, se escogieron las siguientes tecnologías:

4. Firestore como base de datos en la nube para almacenar la información que se pondrá a
   disposición a través de la aplicación web. Al ser una base de datos no relaciónale, permite
   una rápida configuración y flexibilidad, haciendo fácil y rápido el guardado y acceso a la
   información almacenada.
5. Google Cloud Functions para cuando ea necesario ejecutar funciones que hagan
   transformaciones sobre los datos.
6. Tecnología para identificar lenguaje natural que le permita al usuario subir y buscar
   información más fácilmente en el sistema.

La página web contendrá una vista para el experto, y una vista diferente para el aprendíz. En la
vista del experto, se le ofreceran funcionalidades a este para almacenar conocimiento con
respecto a las diferentes métricas que miden el desempeño de las darkstores. En la vista del
aprendíz, se le mostrará a este toda el conocimiento indexado que fue recopilado por el experto de una manera amigable y fácil de encontrar y ¦ltrar
