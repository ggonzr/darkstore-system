/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */

const speech = require("@google-cloud/speech");

// Creates a client
const client = new speech.SpeechClient();

exports.processSpeech = async (req, res) => {
  // Say to request domain all origins are allowed
  res.set("Access-Control-Allow-Origin", "*");

  // If it is a preflight
  if (req.method === "OPTIONS") {
    // Send response to OPTIONS requests
    res.set("Access-Control-Allow-Methods", "POST");
    res.set("Access-Control-Allow-Headers", "Content-Type, Authorization");
    res.set("Access-Control-Max-Age", "3600");
    return res.status(204).send("");
  }

  const config = req.body.config;
  const audio = req.body.audio;

  // Verify if config is correct.
  if (!config) {
    return res.status(400).send({ err: "Configuration params are invalid" });
  }

  // Verify if audio object is correct
  if (!audio) {
    return res.status(400).send({ err: "Audio is invalid" });
  }

  // Audio request
  const audioRequest = {
    config: config,
    audio: audio,
  };

  // Detects speech in the audio file
  // Detects speech in the audio file
  const [response] = await client.recognize(audioRequest);
  const transcription = response.results
    .map((result) => result.alternatives[0].transcript)
    .join("\n");
  console.log(`Transcription: ${transcription}`);

  const responseBody = { text: transcription };
  return res.status(200).send(responseBody);
};
