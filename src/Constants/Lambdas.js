/**
 * Lambda constants
 */

// Endpoint
export const speechToText = process.env.REACT_APP_SPEECH_TO_TEXT || "";

// Endpoint api key
export const gcpApikey = process.env.REACT_APP_GCP_API_KEY || "";


