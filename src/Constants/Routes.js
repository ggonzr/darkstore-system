/**
 * Routes to the different sections views
 */

// Register View
export const register = "register";

// Search Case View
export const searchCase = "searchCase";

// Case Detail View
export const caseDetail = "caseDetail";

export const createCase = "createCase";

// Auth view
export const auth = "auth";

// Home View
export const home = searchCase;

// Test view
export const test = "test";

// Google Map view
export const map = "mapView";
