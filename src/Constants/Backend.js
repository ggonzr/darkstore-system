/**
 * Backend URL and port
 */

// Docker deploy URL
export const BACKEND_URL = "http://172.24.100.62:3000";

// Dev environment URL
//export const BACKEND_URL = "http://localhost:8000";
