/**
 * Possible models to train and predict
 */

// User-User model
export const USER_USER = "Usuario-Usuario";

// Item-Item model
export const ITEM_ITEM = "Item-Item";

// Cosine distance
export const COSINE = "Distancia Coseno";

// Pearson coeficcient
export const PEARSON = "Coeficiente Pearson";

// Jaccard index
export const JACCARD = "Indice Jaccard";

// Backend endpoint names
export const BACKEND_PARAMS = {
  [USER_USER]: "USER_USER",
  [ITEM_ITEM]: "ITEM_ITEM",
  [COSINE]: "cosine",
  [PEARSON]: "pearson_baseline",
  [JACCARD]: "JACCARD",
};
