/**
 * Here, there is an example of the data retrieved from the backend
 * This is design according to the ER diagram
 */

/**
 * This is an example of the Ratings dataset
 * First column is related to the user_id
 * Second column to the artist_id (item)
 * Third column to the score given by the user (rating)
 * 
 user_000001,00c73a38-a449-4990-86ca-5088dde1b8df,2
 user_000001,012a77c9-c897-494f-87d0-0a730996494d,1
 user_000001,014ba96b-b8da-49e3-8a2b-b720ae42e84c,3 
 */

// In this object, we store the data related to the artist
// The key is the artist_id and value its data
export const artist = [
  {
    id: "bd13909f-1c29-4c27-a874-d4aaf27c5b1a",
    name: "Fleetwood Mac",
  },
  {
    id: "2f3dfafb-be37-4d6c-86c7-23d0650497d4",
    name: "Laineen Kasperi",
  },
  {
    id: "b57db116-5947-462e-aa7b-3dd1ecb5be45",
    name: "Necro",
  },
];

// In this object, we store the scores given to an artist
export const rating = [
  {
    user: "ggonzr", // This is the current user id (FK). Here, i set my username
    item: "0001cd84-2a11-4699-8d6b-0abf969c5f06", // This is the artist id (FK)
    artistName: "Midway", // This is the artist id
    rating: 2, // The score given - This score is in (1, 5)
  },
];

// In this object, we store the recommendation
// The example is given but we are not handling this data in the frontend.
// For this purpose, we use Apache Superset.
export const estimation = [
  {
    user: "ggonzr", // This is the current user id (FK). Here, i set my username
    item: "0001cd84-2a11-4699-8d6b-0abf969c5f06", // This is the artist id (FK)
    estimation: 2.5, // The score given by the recommender system.
  },
];

// In this object, we store the user data.
// This represent the active user
export const user = {
  userId: "ggonzr",
  gender: "M", // Gender could be "M" | "F" | ""
  age: 21,
  country: "Colombia", // This could help us to get better insights.
};
