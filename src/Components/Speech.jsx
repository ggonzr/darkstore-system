/**
 * Speech to Text microphone recorder
 * Also, there is a switch to enable/disable form text writing
 */

import { useState, useEffect } from "react";
import { connect } from "react-redux";
import Image from "react-bootstrap/Image";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import translateAudio from "../Services/SpeechToText";
import { registerSpeech } from "../Redux/actions";

// Hooks: Media recorder
/**
 * MediaRecorder component to retrieve an audio buffer stream
 * @param stream - Buffer stream given by WebRTC module
 */
const useMediaRecorder = (stream) => {
  const [stop, setStop] = useState(false);
  const [shouldStop, setShouldStop] = useState(false);
  const [chunks, setChunks] = useState([]);

  const options = { mimeType: "audio/webm" };

  const stopTracks = (stream) => {
    stream
      .getTracks() // get all tracks from the MediaStream
      .forEach((track) => track.stop()); // stop each of them
  };

  // Not valid stream
  if (!stream) {
    return [null, null];
  }

  // Validate if stream is valid
  const mediaRecorder = new MediaRecorder(stream, options);

  /**
   * Retrieves the data as a blob
   * @returns Blob with the audio record
   */
  const getData = () => {
    return new Blob(chunks, { type: "audio/webm" });
  };

  mediaRecorder.addEventListener("dataavailable", function (e) {
    if (e.data.size > 0) {
      // This will execute after stopping the recording.
      setChunks([...chunks, e.data]);
    }

    if (shouldStop === true && stop === false) {
      setStop(true);
    }
  });

  mediaRecorder.addEventListener("stop", function () {
    stopTracks(stream);
    setShouldStop(true);
  });

  return [mediaRecorder, getData];
};

// Component: Speech recording
// FIXME: There is a bug when you want to make more than one recording.
const Speech = ({ id, registerSpeech }) => {
  const [enable, setEnable] = useState(false);
  const [speechStart, setSpeechStart] = useState(false);
  const [translate, setTranslate] = useState(false);
  const [stream, setStream] = useState(null);
  const [mediaRecorder, getData] = useMediaRecorder(stream);

  /**
   * Effects
   */

  // 1: Start recording
  useEffect(() => {
    if (mediaRecorder && speechStart === true && translate === false) {
      mediaRecorder.start();
    }
  }, [mediaRecorder, speechStart, translate]);

  // 2: Start translating
  useEffect(() => {
    const execute = async () => {
      if (translate === true && mediaRecorder) {
        const blob = getData();
        if (blob.size > 0) {
          const text = await translateAudio(blob);
          registerSpeech(id, text);
          // Enable a second audio record
          setStream(null);
          setTranslate(false);
          setSpeechStart(false);
        }
      }
    };
    execute();
  }, [translate, getData, speechStart, mediaRecorder, registerSpeech, id]);

  /**
   * Start the recording
   */
  const handleStart = () => {
    navigator.mediaDevices
      .getUserMedia({ audio: true, video: false })
      .then((navigatorStream) => {
        setStream(navigatorStream);
        setSpeechStart(true);
      });
  };

  /**
   * Stop the recording
   */
  const handleClose = () => {
    mediaRecorder.stop();
    setTranslate(true);
  };

  /**
   * Handle the audio recording.
   */
  const handleRecord = () => {
    // If the record has not started
    if (speechStart === false && translate === false) {
      handleStart();
    }
    // If the record must be stopped
    if (speechStart === true && translate === false) {
      handleClose();
    }
  };

  return (
    <Form.Row className="mb-2">
      <Button className="mx-1" disabled={!enable} onClick={handleRecord}>
        <Image
          src="icons/microphone.svg"
          weight="15px"
          height="15px"
          alt="Start speech"
        />
      </Button>
      <Form.Check
        className="mx-1"
        type="checkbox"
        id="ck-micro"
        label="Enable Speech recording"
        onClick={() => setEnable(!enable)}
      />
    </Form.Row>
  );
};

export default connect(null, { registerSpeech })(Speech);
