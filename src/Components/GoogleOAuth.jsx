/**
 * Google OAuth component to sign-in
 */

import Button from "react-bootstrap/Button";
import firebase from "firebase/app";
import { connect } from "react-redux";
import { auth } from "../Services/Firebase";
import { userInfo } from "../Redux/actions";

const GoogleOAuth = ({ userInfo }) => {
  var provider = new firebase.auth.GoogleAuthProvider();

  /**
   * Handle Google OAuth authentication displaying a pop-up
   */
  const handlePopUp = () => {
    auth
      .signInWithPopup(provider)
      .then((result) => {
        console.log("Result: ", result);
        const credential = result.credential;
        // This gives you a Google Access Token. You can use it to access the Google API.
        const token = credential.accessToken;
        // The signed-in user info.
        const user = result.user;

        // Send token to redux store
        userInfo(token, user);
      })
      .catch((error) => {
        console.error("Error: ", error);
      });
  };

  return (
    <Button
      className="mx-auto mt-3"
      onClick={handlePopUp}
      type="submit"
      variant="primary"
    >
      <img src="icons/google-icon.svg" alt="google icon" />
    </Button>
  );
};

export default connect(null, { userInfo })(GoogleOAuth);
