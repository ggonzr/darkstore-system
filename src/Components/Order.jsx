/**
 * Display an order made in a darkstore
 */

import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Accordion from "react-bootstrap/Accordion";
import "../Styles/Card.css";

const Order = ({ id, data }) => {
  // Display user trayectory using Google Maps
  const handleMap = () => {
    const storeCords = `${data.LAT_STORE},${data.LNG_STORE}`;
    const userCords = `${data.LAT_USER},${data.LNG_USER}`;
    const url = `https://www.google.com/maps/dir/${storeCords}/${userCords}`;
    window.open(url, "_blank");
  };
  return (
    <Accordion key={`order-${data.ORDER_ID}`} id={`order-${data.ORDER_ID}`}>
      <Card className="custom-card mt-2">
        <Accordion.Toggle          
          as={Button}
          variant="link"
          eventKey={`${data.ORDER_ID}`}
        >
          <Card.Header className="text-center custom-card">{`Orden #${id}`}</Card.Header>
        </Accordion.Toggle>
        <Accordion.Collapse eventKey={`${data.ORDER_ID}`}>
          <div>
            <Card.Body>
              <Card.Text>
                Tiempo de entrega: {`${data.TIME_MINUTES} minutos`}
              </Card.Text>
              <Card.Text>Zona de entrega: {`${data.MICROZONE_NAME}`}</Card.Text>
              <Card.Text>
                Fecha del pedido: {`${data.CLOSED_AT}`}
              </Card.Text>
              <Card.Text>
                Tipo de compra: {`${data.VERTICAL_SUB_GROUP}`}
              </Card.Text>
              <Card.Text>
                Valor en USD: {`${data.GMV_USD.toFixed(2)}`}
              </Card.Text>
            </Card.Body>
            <Card.Footer className="text-muted">
              <Button variant="primary" onClick={handleMap}>
                Ver trayectoria estimada
              </Button>
            </Card.Footer>
          </div>
        </Accordion.Collapse>
      </Card>
    </Accordion>
  );
};

export default Order;
