/**
 * Display a country with its stores
 */

import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Accordion from "react-bootstrap/Accordion";
import "../Styles/Card.css";

const Country = ({ country, children }) => {
  return (
    <div>
      <Accordion key={`accordion-country-${country}`} id={`accordion-country-${country}`}>
        <Card className="custom-card">
          <Accordion.Toggle as={Button} variant="link" eventKey={country}>
            <Card.Header className="custom-card">{country}</Card.Header>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey={country}>
            <Card.Body>{children}</Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </div>
  );
};

export default Country;
