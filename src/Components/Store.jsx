/**
 * Display a store with its orders
 */

import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Accordion from "react-bootstrap/Accordion";
import "../Styles/Card.css";

const Store = ({ store, children }) => {
    return (
        <div>
            <Accordion key={`accordion-store-${store}`} id={`accordion-store-${store}`}>
                <Card className="second-background">
                    <Accordion.Toggle as={Button} variant="link" eventKey={store}>
                        <Card.Header className="custom-card">{store}</Card.Header>
                    </Accordion.Toggle>
                    <Accordion.Collapse eventKey={store}>
                        <Card.Body>{children}</Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        </div>
    );
};

export default Store;