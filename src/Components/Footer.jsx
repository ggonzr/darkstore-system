/**
 * Footer - Author information
 */

import Navbar from "react-bootstrap/Navbar";
import "../Styles/Footer.css";

export const Footer = () => {
  return (
    <Navbar
      className="custom-footer"                     
      bg="dark"
      variant="dark"      
    >
      <Navbar.Collapse className="justify-content-center">
        <Navbar.Text>
          <i className="fab fa-creative-commons"></i> Chipper & Avocado KBS - Made with{" "}
          <i className="far fa-heart"></i> by{" "}
          <a href="https://github.com/ggonzr" className="mx-2" target="_blank" rel="noreferrer">
            Geovanny González-Rodríguez            
          </a>
          <a href="https://github.com/juanesmendez" target="_blank" rel="noreferrer">
            Juan Esteban Mendez
          </a>
        </Navbar.Text>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Footer;
