/**
 * Top navigation bar (NavBar)
 */

import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { userInfo } from "../Redux/actions";
import { getUserData } from "../Redux/selectors";
import { NavbarUserInfo as NavbarInfo } from "./NavbarUserInfo";
import { Container } from "react-bootstrap";
import { auth } from "../Services/Firebase";
import { auth as authRoute, searchCase, map } from "../Constants/Routes";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import "../Styles/NavBar.css";

const NavBar = ({ userInfo, userData }) => {
  const history = useHistory();

  /**
   * Close Firebase session and redirect user to auth view
   */
  const handleCloseSession = () => {
    auth
      .signOut()
      .then((resolve) => {
        userInfo("", null);
        history.replace(`/${authRoute}`);
      })
      .catch((err) => {
        console.error("[HandleCloseSession] Error: ", err);
      });
  };

  /**
   * Handles page redirect without refresh the page
   * @param {*} route Route to redirect.
   */
  const handleRedirect = (route) => {
    history.push(`/${route}`);
  };

  /**
   * Display nav links to navigate through the page
   */
  const handleLinks = () => {
    if (userData.user) {
      return (
        <Nav className="mr-auto">
          <Nav.Link key={`search-cases-link`} onClick={() => handleRedirect(searchCase)}>
            Buscar casos
          </Nav.Link>
          <Nav.Link key={`map-view-link`} onClick={() => handleRedirect(map)}>
            Mapa de tiendas
          </Nav.Link>
        </Nav>
      );
    }
    return null;    
  };

  /**
   * Display user information and close session button
   * @returns User's information component.
   */
  const handleInfo = () => {
    if (!userData.user) {
      return null;
    } 
    return (
      <NavbarInfo
        className="d-flex justify-content-end"
        userData={userData}
        handleCloseSession={handleCloseSession}
      />
    );
  };

  return (
    <Navbar sticky="top" expand="xl" bg="dark" variant="dark">
      <Container fluid>
        <Navbar.Brand>
          <img
            alt="Logo"
            src="images/shops.svg"
            width="32px"
            height="32px"
            className="d-inline-block align-top custom-navbar"
          />
          Chipper & Avocado KBS
        </Navbar.Brand> 
        {handleLinks()}       
        {handleInfo()}        
      </Container>
    </Navbar>
  );
};

export default connect((store) => ({ userData: getUserData(store) }), {
  userInfo,
})(NavBar);
