/**
 * Right label to show user's info and close session button
 */

import Navbar from "react-bootstrap/Navbar";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

export const NavbarUserInfo = ({ userData, handleCloseSession }) => {
  const handleInfo = () => {
    let name = "";
    if (!userData.user) {
      return null;
    }
        
    const uName = userData.user.displayName || "";
    const uEmail = userData.user.email || "";

    if (uName) {
      name = userData.user.displayName
    }
    else if (uEmail) {
      name = userData.user.email
    }
    
    return (
    <Navbar.Text
      className="custom-navbar-user-info"
      key="navbar-link-admin-user"
    >
      Bienvenido estimado {name}
    </Navbar.Text>);    
  };

  return (
    <Form inline>
      {handleInfo()}
      <Button variant="outline-info" onClick={handleCloseSession}>
        Cerrar Sesión
      </Button>
    </Form>
  );
};

export default NavbarUserInfo;
