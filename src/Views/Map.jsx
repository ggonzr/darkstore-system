/**
 * Map component to see all darkstores locations
 */

import { useState } from "react";
import Container from "react-bootstrap/Container";
import Order from "../Components/Order";
import Store from "../Components/Store";
import Country from "../Components/Country";
import { sendData } from "../Services/Data";
import { Accordion } from "react-bootstrap";

const Map = () => {
  const [data] = useState(sendData());

  // Display all data
  const displayData = () => {
    let cComponentList = [];
    for (let [country, stores] of Object.entries(data)) {
      const cName =
        country === "BR"
          ? "Avocado - Brasil - São Paulo"
          : "Chipper - México - Ciudad de México";
      let sComponentList = [];
      for (let [store, orders] of Object.entries(stores)) {
        const oComponent = orders.map((el, idx) => (
          <Order key={`order-${el.ORDER_ID}`} id={idx} data={el} />
        ));
        const sComponent = (
          <Store key={`store-${store}`} store={store}>
            {oComponent}
          </Store>
        );
        sComponentList.push(sComponent);
      }
      const cComponent = (
        <Country key={`country-${country}`} country={cName}>
          {sComponentList}
        </Country>
      );
      cComponentList.push(cComponent);
    }
    return cComponentList;
  };

  return (
    <Container className="mt-2 mb-2">
      <h1 className="text-center">Pedidos realizados</h1>
      <Accordion>{displayData()}</Accordion>
    </Container>
  );
};

export default Map;
