import React from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { Container } from "react-bootstrap";
import { getTextFromSpeech } from "../Redux/selectors";
import { firestore, storage } from "../Services/Firebase";
import { searchCase } from "../Constants/Routes";

import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Speech from "../Components/Speech";

const CreateCase = ({ speechs }) => {
  // References to all form fields
  const tituloCaseRef = React.useRef(null);
  const descriptionCaseRef = React.useRef(null);
  const conclusionCaseRef = React.useRef(null);
  const palabrasCaseRef = React.useRef(null);
  const fileUploadRef = React.useRef(null);

  // Browser router
  const history = useHistory();

  // Effects: Get translated text
  React.useEffect(() => {
    if (speechs["description-speech"]) {
      // Assign the text
      descriptionCaseRef.current.value = speechs["description-speech"];
    }
    if (speechs["conclusion-speech"]) {
      conclusionCaseRef.current.value = speechs["conclusion-speech"];
    }
  }, [speechs]);

  /**
   * Uploads the files to the Google Storage bucket
   * @param {*} id Reference to the case document id
   * @returns A list with the URLs to be saved in the document.
   */
  const handleFiles = async (id) => {
    const files = fileUploadRef.current.files;
    let urls = [];
    try {
      for (let file of files) {
        const reference = `${id}/${file.name}`;
        const fRef = storage.ref(reference);
        await fRef.put(file);
        const url = await fRef.getDownloadURL();
        urls.push(url);
      }
      return urls;
    } catch (err) {
      console.error("[CreateCase][handleFiles()] Error: ", err);
    }
  };

  /**
   * Creates a new document in Cloud Firestore
   * @param {*} body Document to be added
   */
  const handleCreation = async (body) => {
    try {
      const docRef = firestore.collection("cases").doc();
      const fileUrls = await handleFiles(docRef.id);
      const newBody = {
        ...body,
        files: fileUrls,
      };
      await docRef.set(newBody);
      history.replace(`${searchCase}`);
    } catch (error) {
      console.error("[CreateCase][handleCreation()] Error: ", error);
    }
  };

  // Submit the request.
  const handleSubmit = (event) => {
    event.preventDefault();
    console.log("In handleSubmit");

    const titleCase = tituloCaseRef.current.value;
    const descriptionCase = descriptionCaseRef.current.value;
    const conclusionCase = conclusionCaseRef.current.value;
    const palabrasClaveCase = palabrasCaseRef.current.value;

    const document = {
      title: titleCase,
      description: descriptionCase,
      conclusion: conclusionCase,
      keywords: palabrasClaveCase,
    };

    if (
      titleCase !== "" &&
      descriptionCase !== "" &&
      conclusionCase !== "" &&
      palabrasClaveCase !== ""
    ) {
      handleCreation(document);
    } else {
      alert("Se deben llenar todos los campos");
    }
  };

  return (
    <Container fluid className="mb-4 custom-admin-card">
      <Row className="d-flex justify-content-center">
        <Col md={8}>
          <Card>
            <Card.Header>
              <Card.Title>Crear caso</Card.Title>
            </Card.Header>

            <Card.Body>
              <Form onSubmit={handleSubmit}>
                <Form.Group controlId="form.ControlInput1">
                  <Form.Label>Título del caso</Form.Label>
                  <Form.Control
                    ref={tituloCaseRef}
                    type="text"
                    placeholder="Posicionamiento de una nueva darkstore"
                  />
                </Form.Group>
                <Form.Group controlId="form.ControlDescripcion">
                  <Form.Label>Descripción</Form.Label>
                  <Speech id={"description-speech"} />
                  <Form.Control
                    ref={descriptionCaseRef}
                    as="textarea"
                    rows={3}
                  />
                </Form.Group>
                <Form.Group controlId="form.ControlConclusion">
                  <Form.Label>Conclusión</Form.Label>
                  <Speech id={"conclusion-speech"} />
                  <Form.Control
                    ref={conclusionCaseRef}
                    as="textarea"
                    rows={3}
                  />
                </Form.Group>
                <Form.Group controlId="form.ControlPalabras">
                  <Form.Label>Palabras clave</Form.Label>
                  <Form.Control ref={palabrasCaseRef} as="textarea" rows={2} />
                </Form.Group>
                <Form.Group controlId="form.ControlPalabras">
                  <Form.Label>Soportes</Form.Label>
                  <Form.File ref={fileUploadRef} multiple={"multiple"} />
                </Form.Group>
                <Button variant="primary" type="submit">
                  Crear
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default connect(
  (store) => ({
    speechs: getTextFromSpeech(store),
  }),
  null
)(CreateCase);
