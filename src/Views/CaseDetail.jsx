
import { useLocation } from "react-router-dom";
import { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";

import { firestore } from "../Services/Firebase";

export const CaseDetail = () => {
  const [caseData, setCaseData] = useState([]);
  const location = useLocation();

  // Effects: Retrieve case info from database
  useEffect(() => {
    // Se recibe el ID del CASE que se desea mostrar
    console.log(location.state.id); // result: '/secondpage'

    // fetch data from the specific Case using its ID
    firestore.collection('cases').doc(location.state.id).get()
      .then((doc) => {
        // doc.data() is never undefined for query doc snapshots
        console.log(doc.id, " => ", doc.data());
        setCaseData(doc.data());
      })
      .catch((error) => {
        console.log("Error getting document: ", error);
      });
  }, [location]);

  /**
   * Retrieves the image components to display all support data
   */
  const handleSupport = () => {
    if (caseData.files) {
      const images = caseData.files.map((url) => {
        return (
          <Image 
            src={url}
            thumbnail
          />
        );
      });
      return images      
    }
    else {
      return null;
    }
  };

  return (
    <Container fluid className="mb-4 custom-admin-card">
      <Row className="d-flex justify-content-center">
        <Col md={6}>
          <div>
            <Card bg="light">
              <Card.Body>
                <Card.Title>
                  Titulo del caso: {caseData.title}
                </Card.Title>
              </Card.Body>
            </Card>
          </div>
          <br></br>
          <div>
            <Card bg="light">
              <Card.Body>
                <Card.Title>Descripcion</Card.Title>
                <Card.Text>
                 {caseData.description}
                </Card.Text>
                <Card.Title>Conclusiones</Card.Title>
                <Card.Text>
                  {caseData.conclusion}
                </Card.Text>
              </Card.Body>
            </Card>
          </div>
        </Col>
        <Col md={6}>
          <div>
            <Card bg="light">
              <Card.Body>
                <Card.Title>Palabras Clave: {caseData.keywords}</Card.Title>
              </Card.Body>
            </Card>
          </div>
          <br></br>
          <div>
            <Card bg="light">
              <Card.Body>
                <Card.Title>Soportes</Card.Title>
                {handleSupport()}                
              </Card.Body>
            </Card>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default CaseDetail;
