/**
 * Main Dashboard (Groups all components !)
 */

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { map, auth, caseDetail, register, searchCase, createCase } from "../Constants/Routes";
import Navbar from "../Components/NavBar";
import Auth from "./Auth";
import Cases from "./Cases";
import Footer from "../Components/Footer";
import Register from "./Register";
import Map from "./Map";
import { CaseDetail } from "./CaseDetail";
import CreateCase from "./CreateCase";
import "../Styles/Dashboard.css";


const Dashboard = () => {
  //Code section in Switch renders the routes to access each section data
  return (
    <Router>
      <div>
        <Navbar />
        <Switch>
          <Route exact path="/">
            <Redirect to={`/${auth}`} />
          </Route>
          <Route
            key="admin-access-panel"
            path={`/${auth}`}
            exact
            children={() => <Auth />}
          />
          <Route path={`/${register}`} exact children={() => <Register />} />
          <Route
            key="search-dashboard"
            path={`/${searchCase}`}
            exact
            children={() => <Cases />}
          />
          <Route
            key="case-detail"
            path={`/${caseDetail}`}
            exact
            children={() => <CaseDetail />}
          />
          <Route
            key="map-view"
            path={`/${map}`}
            exact
            children={() => <Map />}
          />
          <Route
            key="create-case"
            path={`/${createCase}`}
            exact
            children={() => <CreateCase />}
          />
        </Switch>
        <Footer />
      </div>
    </Router>
  );
};

export default Dashboard;
