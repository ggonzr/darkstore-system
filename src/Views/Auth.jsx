/**
 * Admin Access
 */
import { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Container } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { home, register } from "../Constants/Routes";
import { userInfo } from "../Redux/actions";
import { getUserData } from "../Redux/selectors";
import { auth } from "../Services/Firebase";
import GoogleOAuth from "../Components/GoogleOAuth";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "../Styles/AdminAccess.css";

// Properties de-structuration. The props function userInfo is the action
// wrapped by React-Redux not the pure function.
export const Auth = ({ userData, userInfo }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [validated] = useState(false);
  const history = useHistory();

  //Effects

  //Is the user already logged-in ?
  useEffect(() => {
    if (userData.user) {
      history.push(`/${home}`);
    }
  }, [history, userData]);

  const handleRegister = (event) => {
    event.preventDefault();
    event.stopPropagation();
    history.push(`/${register}`);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    auth
      .signInWithEmailAndPassword(email, password)
      .then((result) => {
        const credential = result;
        // This gives you a Google Access Token. You can use it to access the Google API.
        const token = credential.accessToken;
        // The signed-in user info.
        const user = result.user;

        // Update user
        userInfo(token, user);
      })
      .catch((err) => {
        console.error("[Auth][HandleSubmit] Error: ", err);
        event.preventDefault();
        event.stopPropagation();
      });
  };

  return (
    <Container fluid className="mb-4 custom-admin-card">
      <Col className="d-flex justify-content-center">
        <Row className="d-flex justify-content-center">
          <Card
            bg="dark"
            key="admin-access"
            text="white"
            style={{ width: "18rem" }}
            className="d-flex justify-content-center mb-2"
          >
            <Card.Header>Authentication</Card.Header>
            <Card.Body>
              <Card.Title>Iniciar Sesión </Card.Title>
              <Form
                className="custom-form"
                noValidate
                validated={validated}
                onSubmit={handleSubmit}
              >
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Correo electronico</Form.Label>
                  <Form.Control
                    required
                    type="text"
                    placeholder="Ingrese su email"
                    onChange={(ev) => setEmail(ev.target.value)}
                  />
                  <Form.Control.Feedback type="invalid">
                    Por favor ingrese su nombre de usuario
                  </Form.Control.Feedback>
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                  <Form.Label>Contraseña</Form.Label>
                  <Form.Control
                    required
                    type="password"
                    placeholder="Password"
                    onChange={(ev) => setPassword(ev.target.value)}
                  />
                  <Form.Control.Feedback type="invalid">
                    Por favor ingrese su contraseña
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Row className="mx-auto">
                  <Button className="mx-auto" type="submit" variant="primary">
                    Ingresar
                  </Button>
                  <Button
                    className="mx-auto"
                    onClick={handleRegister}
                    type="submit"
                    variant="primary"
                  >
                    Registrarse
                  </Button>
                </Form.Row>                
              </Form>
              <Row xs className="mx-auto">
                <GoogleOAuth />
              </Row>
            </Card.Body>
          </Card>
        </Row>
      </Col>
    </Container>
  );
};

export default connect(
  (store) => ({
    userData: getUserData(store),
  }),
  { userInfo }
)(Auth);
