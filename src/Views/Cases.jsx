import { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Container } from "react-bootstrap";
import { userInfo } from "../Redux/actions";
import { useHistory } from 'react-router-dom';


import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Table from "react-bootstrap/Table";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";

import { firestore } from "../Services/Firebase";

import { caseDetail, createCase } from "../Constants/Routes";

export const Cases = ({ userInfo }) => {
  const [cases, setCases] = useState([]);
  const [keywords, setKeyWords] = useState([]);
  const [keywordToAdd, setKeywordToAdd] = useState("");

  const history = useHistory();

  useEffect(() => {
    queryCases();
  }, []);

  const queryCases = () => {
    //console.log(firestore);
    //console.log("Inside query cases from firestore");
    var newCases = [];

    firestore.collection("cases")
      .get()
      .then((querySnapshot) => {        
        querySnapshot.forEach((doc) => {
          // doc.data() is never undefined for query doc snapshots
          //console.log(doc.id, " => ", doc.data());
          newCases.push({
            id: doc.id,
            title: doc.data().title,
            keywords: doc.data().keywords,
            description: doc.data().description,
            conclusion: doc.data().conclusion
          });
        });
        setCases(newCases);
        //console.log(cases);
      })
      .catch((error) => {
        console.error("[Cases][queryCases()] Error getting documents: ", error);
      });
  }


  const handleAddKeyword = () => {
    //console.log(`Agregando la palabra ${keywordToAdd}`);
    var newKeyWords = keywords;
    newKeyWords.push(keywordToAdd);
    setKeyWords(newKeyWords);
    //console.log(keywords);
    // Dejar vacio el keywordToAdd
    setKeywordToAdd("");
  };

  const handleCaseDetail = (event) => {
    const button = event.currentTarget;
    //console.log(event);
    //console.log(button.id);
    history.push({
      pathname: `/${caseDetail}`,
      state: {
        id: button.id
      }
    });
  }

  const handleCreateCase = (event) => {
    history.push(`${createCase}`);
  }

  return (
    <Container fluid className="mb-4 custom-admin-card">
      <Row>
        <Col md={10}></Col>
        <Col md={2}>
          <Button
            variant="success"
            onClick={handleCreateCase}
          >
            Crear caso
          </Button>
        </Col>
      </Row>
      <br></br>
      <Row className="d-flex justify-content-center">
        <Col md={8}>
          <Card bg="light">
            <Card.Header>
              <h2>Palabras Clave</h2>
            </Card.Header>
            <Card.Body>
              <h4>Por favor ingrese los términos de busqueda</h4>
              <InputGroup className="mb-3">
                <FormControl
                  placeholder="Palabra clave"
                  aria-label="Palabra clave"
                  aria-describedby="basic-addon2"
                  value={keywordToAdd}
                  onChange={(e) => setKeywordToAdd(e.target.value)}
                />
                <InputGroup.Append>
                  <Button
                    variant="outline-secondary"
                    onClick={handleAddKeyword}
                  >
                    Agregar
                  </Button>
                </InputGroup.Append>
              </InputGroup>
              <div>
                {keywords.map((e, i) => (
                  <Card key={i}>
                    <Card.Body>{e}</Card.Body>
                  </Card>
                ))}
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      <br></br>
      <Row className="d-flex justify-content-center">
        <Col>
          <Card bg="light">
            <Card.Header>
              <h2>Resultados</h2>
            </Card.Header>
            <Card.Body>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Palabras Clave</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {cases.map((e, i) => (
                    <tr key={`row-${i}`}>
                      <td>{i + 1}</td>
                      <td>
                        {e.title}
                      </td>
                      <td>{e.keywords}</td>
                      <td className="justify-content-center">
                        <Button onClick={handleCaseDetail} id={e.id}>Revisar</Button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default connect(null, { userInfo })(Cases);
