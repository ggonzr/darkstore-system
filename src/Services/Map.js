/**
 * Map services
 */

import { gcpApikey } from "../Constants/Lambdas";
import axios from "axios";

// Google Maps URL
const googleMapsUrl = "https://maps.googleapis.com/maps/api/js";

// Get map authorization
export const getMap = () => {
  return axios.get(`${googleMapsUrl}?key=${gcpApikey}`);
};
