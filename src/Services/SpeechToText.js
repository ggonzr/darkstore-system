/**
 * Speech to text service to parse audio to text
 */

import axios from "axios";
import { speechToText, gcpApikey } from "../Constants/Lambdas";

/**
 * Parse the blob with the audio to base 64 encoding string
 * @param {*} blob Audio blob to be parsed
 * @returns The base64 representation
 */
const parseBlobToBase64 = (blob) => {
  return new Promise((resolve, reject) => {
    const fr = new FileReader();
    fr.onload = function () {
      const base64 = fr.result;
      let response = base64.substring(base64.indexOf(",") + 1);
      resolve(response);
    };
    fr.onerror = function () {
      reject(fr.error);
    };
    fr.readAsDataURL(blob);
  });
};

/**
 * Translate the audio to text
 * @param {*} blob Audio blob to be parsed
 * @returns The translation of the audio to text
 */
const translateAudio = async (blob) => {
  try {
    const url = `${speechToText}?key=${gcpApikey}`;
    const base64Encoding = await parseBlobToBase64(blob);
    const body = {
      config: {
        languageCode: "es-ES",
      },
      audio: {
        content: base64Encoding,
      },
    };
    console.log("[translateAudio] Body: ", body);
    const headers = {
      "Content-Type": "application/json",
    };
    const result = await axios.post(url, body, headers);
    const { text } = result.data;
    return text;
  } catch (err) {
    console.error("[SpeechToText][translateAudio()] Error: ", err);
    return "NO_DATA";
  }
};

export default translateAudio;
