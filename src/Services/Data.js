import { data } from "../Constants/Data";

export const sendData = () => {
  let res = {};
  for (const [country, stores] of Object.entries(data)) {
    for (const [store, orders] of Object.entries(stores)) {
      const newOrders = orders.slice(0, 5);
      const c = res[country] || {};
      // Add the array
      c[store] = newOrders;
      res = { ...res, [country]: c };
    }
  }
  return res;
};
