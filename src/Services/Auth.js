/**
 * Authentication service using JsonWebTokens
 */

import jwt from "jsonwebtoken";
import axios from "axios";
import { BACKEND_URL } from "../Constants/Backend";

const API_KEY = "profeLoConfigureConCIPeroNiAsiTomaLaVariable";
const PARSE_JWT_TOKEN = process.env.REACT_APP_secret || API_KEY;

/**
 * Creates a new token for the current session
 * @param {*} userData User data to be saved in the token's payload
 */
export const createToken = (userData) => {
  //JWT Secret Key
  const jwtSecret = PARSE_JWT_TOKEN;

  //JWT String Token
  const newToken = jwt.sign(userData, jwtSecret, {
    expiresIn: "30min",
  });

  //Save new token
  localStorage.setItem("token", newToken);

  return newToken;
};

/**
 * Verifies if the current token saved in browser's local storage is valid.
 */
export const verifyToken = () => {
  let jwtSecret;
  let currentToken;
  try {
    //JWT Secret Key
    jwtSecret = PARSE_JWT_TOKEN;

    //Token
    currentToken = localStorage.getItem("token");

    //Decoded payload
    const payload = jwt.verify(currentToken, jwtSecret);

    return payload;
  } catch (error) {
    if (!currentToken) {
      // Token value is undefined (Not exist)
      return false;
    } else {
      // Token not found
      console.error(
        "[VerifyToken] Invalid token provided. The last is due to the user logged out or user has not been logged in"
      );
    }

    // Token secret key not provided
    if (!jwtSecret) {
      console.error("[VerifyToken] JWT Secret Key not provided !");
    }
    return false;
  }
};

/**
 * Delete the current token in browser's local storage
 */
export const deleteSession = () => {
  localStorage.removeItem("token");
};

/**
 * Auth an user from backend
 * @param userInfo Username and password
 */

export const authUser = (username, password) => {
  const userInfo = {
    username: username,
    password: password,
  };
  return axios.post(`${BACKEND_URL}/login`, userInfo);
};

/**
 * Register a new user
 * @param {*} username Username to register
 * @param {*} password Password to register
 */
export const registerUser = (username, password) => {
  const userInfo = {
    username: username,
    password: password,
  };
  return axios.post(`${BACKEND_URL}/users/`, userInfo);
};
