/**
 * Enable Firebase services
 */

import { firebaseConfig } from "../Constants/Firebase";
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

// Start Firebase services
const app = firebase.initializeApp(firebaseConfig);

// Services
export const auth = firebase.auth(app);
export const firestore = firebase.firestore(app);
export const storage = firebase.storage(app);

// Set user's language
auth.useDeviceLanguage();
