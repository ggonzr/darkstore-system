/**
 * Name of the actions to dispatch
 */

// Update user info when a log-in or register event occurs
export const USER_INFO = "USER_INFO";

// Register a translation
export const ADD_SPEECH_TRANSLATION = "ADD_SPEECH_TRANSLATION";
