/**
 * User info reducers to update the current user data when he/she logs-in
 * or register in the app.
 */

import { USER_INFO } from "../actionTypes";

// Initial state for user info
const initialState = {
  token: "",
  user: null,
};

const userInfo = (state = initialState, action) => {
  switch (action.type) {
    // Action not recognized
    case USER_INFO: {
      const { token, user } = action.payload;
      return {
        ...state,
        token: token,
        user: user,
      };
    }
    default: {
      return state;
    }
  }
};

export default userInfo;
