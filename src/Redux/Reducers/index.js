/**
 * Reducers: They are used to give the next state of the app given an action
 * an its payload. Here, we are going to combine all reducers in one object
 */

import { combineReducers } from "redux";
import userInfo from "./userReducers";
import speechRegister from "./speechReducer";

export default combineReducers({
  userInfo,
  speechRegister,
});
