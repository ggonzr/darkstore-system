/**
 * Register all speech recorders
 */

import { ADD_SPEECH_TRANSLATION } from "../actionTypes";

const initialState = {};

const speechRegister = (state = initialState, action) => {
  switch (action.type) {
    case ADD_SPEECH_TRANSLATION: {
      const { id, text } = action.payload;
      return {
        ...state,
        [id]: text,
      };
    }
    default: {
      return state;
    }
  }
};

export default speechRegister;
