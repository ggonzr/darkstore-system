/**
 * Selectors: The main objective of selectors is retrieve specific information
 * from the store.
 */

// Retrieve username and password
export const getUserData = (store) => store.userInfo;

// Retrieve text from speech
export const getTextFromSpeech = (store) => store.speechRegister;
