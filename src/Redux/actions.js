/**
 * Actions to dispatch
 */

import { USER_INFO, ADD_SPEECH_TRANSLATION } from "./actionTypes";

// Change current user info: Username and password
export const userInfo = (token, user) => ({
  type: USER_INFO,
  payload: { token: token, user: user },
});

// Register an speech translation
export const registerSpeech = (id, text) => ({
  type: ADD_SPEECH_TRANSLATION,
  payload: { id: id, text: text },
});
