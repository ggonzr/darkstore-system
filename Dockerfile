FROM node:lts-alpine3.12

COPY . .

RUN npm i

RUN npm run build

CMD [ "npm", "run", "docker" ]